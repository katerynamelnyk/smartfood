'use client';

import { configureStore, createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        isAuth: false,
    },
    reducers: {
        login: (state) => {
            state.isAuth = true;
        },
        logout: (state) => {
            state.isAuth = false;
        },
    },
});

export const authActions = authSlice.actions;


export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
    }
});

export type AuthState = ReturnType<typeof store.getState>;