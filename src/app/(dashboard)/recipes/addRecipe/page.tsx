'use client'

import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { RecipeCategory, IngredientName, Measure } from '@prisma/client';
import DropDown from '@/components/UI/DropDown';


const AddRecipe = () => {
    const router = useRouter();

    const handleAddIngredientItem = () => {

    }

    const handleBackButton = () => {
        router.back();
    }

    const handleAddRecipe = (e: React.FormEvent) => {
        e.preventDefault();
    }

    /*
    ID 
    TITLE --- by user
    DESCRIPTION --- by user
    CATEGORY  -> RecipeCategory --- by user
    USER
    USER ID
    INGREDIENTS  -> --- by user
    */


    /*
    add first ingredient
    press Add ingredient Button
       the form in empty
       the added data is rendered in the block under the form
    */

    const addIngredientsBlock = (
        <div>
            <div>
                <DropDown
                    name="ingredients"
                    title="Choose ingredient"
                    options={Object.values(IngredientName)}
                />
                <div>
                    <label htmlFor="amount">Add amount</label>
                    <input id="amount" type="number" />
                </div>
                <DropDown
                    name="measure"
                    title="Choose measure"
                    options={Object.values(Measure)}
                />
            </div>
            <button onClick={handleAddIngredientItem}>Add ingredient</button>
        </div>
    );

    const addedIngredients = <div>Added ingredients</div>

    return (
        <>
            <button onClick={handleBackButton}>BACK</button>
            <div>
                <h2>Add Your Own Recipe</h2>
                <form onSubmit={handleAddRecipe}>
                    <div>
                        <label htmlFor="title">Title:</label>
                        <input id="title" type="text" />
                    </div>
                    <div>
                        <label htmlFor="description">Description:</label>
                        <input id="description" type="text" />
                    </div>
                    <div>
                        <DropDown
                            name="category"
                            title="Choose category"
                            options={Object.values(RecipeCategory)}
                        />
                    </div>
                    <h3>Add Ingredients</h3>
                    {addIngredientsBlock}
                    {addedIngredients}
                    <div>
                        <button>Submit</button>
                    </div>
                </form>
            </div>
        </>
    )
}

export default AddRecipe;