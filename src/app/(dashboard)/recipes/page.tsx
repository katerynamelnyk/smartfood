"use client";

import { RecipeCategory, Measure, IngredientName } from "@prisma/client";
import { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";

type Ingredient = {
  id: string;
  name: IngredientName;
  amount: number;
  measure: Measure;
};

type Recipe = {
  id: string;
  title: string;
  desc: string;
  category: RecipeCategory;
  ingredients: Ingredient[];
};

const RecipeItem = ({ title, desc, category, ingredients }: Recipe) => {
  const renderedIngredients = ingredients?.map((item) => (
    <div key={item.id}>
      <p>{item.name}</p>
      <p>{item.amount}</p>
      <p>{item.measure}</p>
    </div>
  ));
  return (
    <div>
      <h2>{title}</h2>
      <p>{category}</p>
      <p>{desc}</p>
      {renderedIngredients}
    </div>
  );
};

const Recipes = () => {
  const [recipes, setRecipes] = useState<Recipe[]>();

  const { data } = useSession();

  const router = useRouter();

  useEffect(() => {
    (async () => {
      const response = await fetch("api/recipe");
      const data = await response.json();
      console.log(data);
      setRecipes(data);
    })();
  }, []);

  const renderedRecipes = recipes?.map((recipe) => (
    <RecipeItem
      key={recipe.id}
      id={recipe.id}
      title={recipe.title}
      category={recipe.category}
      desc={recipe.desc}
      ingredients={recipe.ingredients}
    />
  ));

  const handleAddRecipe = () => {
    router.push("/recipes/addRecipe");
  };

  const addRecipeButton = (
    <button onClick={handleAddRecipe}>add your own recipe</button>
  );

  return (
    <section>
      {data?.user && addRecipeButton}
      {renderedRecipes}
    </section>
  );
};

export default Recipes;
