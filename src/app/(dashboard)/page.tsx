"use client";

import Link from "next/link";
import Image from "next/image";
import { useSession } from "next-auth/react";

import styles from "./page.module.scss";

const Home = () => {
  const { data } = useSession(); //user info
  console.log(data);

  const signInBtn = (
    <button className={styles.button}>
      <Link href="/signin" className={styles.link}>
        sign in
      </Link>
    </button>
  );

  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <div className={styles.logo}>
          <p className={styles["logo-first"]}>Smart</p>
          <p className={styles["logo-second"]}>Eating</p>
        </div>
        <Image src="/pictures/1.jpg" alt="food" height={220} width={234} />
        <Image src="/pictures/2.jpg" alt="food" height={220} width={234} />
        <Image src="/pictures/3.jpg" alt="food" height={220} width={234} />
        <Image src="/pictures/4.jpg" alt="food" height={220} width={234} />
        {!data?.user && signInBtn}
      </div>
      <div className={styles.remark}>You are what you eat.</div>
    </main>
  );
};

export default Home;
