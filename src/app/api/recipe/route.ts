import prisma from "@/lib/prisma";
import { RecipeCategory } from "@prisma/client";

type PostRequestBody = {
    title: string,
    desc: string,
    category: RecipeCategory,

}

export const GET = async (request: Request) => {
    const recipies = await prisma.recipe.findMany({
        include: {
            ingredients: true,
        }
    });

    return new Response(JSON.stringify(recipies), {
        status: 200
    })
}


export const POST = async (request: Request) => {
    const body: PostRequestBody = await request.json();
    await prisma.recipe.create({
        data: {
            title: body.title,
            desc: body.desc,
            category: body.category,
        }
    })
}