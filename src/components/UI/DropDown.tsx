import React from 'react'

type Props = {
    name: string,
    title: string,
    options: string[],
}

const DropDown = (props: Props) => {
    const renderedOptions = props.options
        .map((option, i) => (
            <option
                key={i}
                value={option}
            >
                {option}
            </option>
        ));

    return (
        <>
            <label htmlFor={props.name}>{props.title}</label>
            <select id={props.name}>
                {renderedOptions}
            </select>
        </>
    )
}

export default DropDown;