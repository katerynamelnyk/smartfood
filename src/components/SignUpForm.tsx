'use client'
import { useSession, signOut, signIn } from 'next-auth/react';


const SignUpForm = () => {
    const {data:session} = useSession();

    if(session && session.user) {
        return (
            <div>
                <button onClick={() => signOut()}>Sign out</button>
            </div>
        )
    };

    return (
        <button onClick={() => signIn()}>Sign in</button>
    )

}

export default SignUpForm;