'use client'
import { useSession } from 'next-auth/react';
import Link from "next/link";

import styles from './Navigation.module.scss';

const Navigation = () => {
    const { data: session } = useSession();

    const optionalNavigation = session?.user
        && <Link className={styles.link} href="/diary">Diary</Link>;

    // const userInfo = session?.user
    //     ? <div className={styles['user-info']}>{session?.user.name}</div>
    //     : <div></div>

    return (
        <header className={styles.header}>
            <nav className={styles.navigation}>
                <div className={styles['optional-links']}>
                    <Link className={styles.link} href="./">Home</Link>
                    <Link className={styles.link} href="/aboutus">About Us</Link>
                    <Link className={styles.link} href="/recipes">Recipes</Link>
                    {optionalNavigation}
                </div>
                {/* {userInfo} */}
                <div>EN/UA</div>
            </nav>
        </header>
    );
}

export default Navigation;