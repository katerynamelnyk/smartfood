/*
  Warnings:

  - You are about to drop the column `ingredients` on the `Recipe` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "IngredientName" AS ENUM ('cheese', 'eggs', 'flour', 'baking_powder');

-- CreateEnum
CREATE TYPE "Measure" AS ENUM ('spoon', 'item', 'cup');

-- AlterTable
ALTER TABLE "Recipe" DROP COLUMN "ingredients";

-- DropEnum
DROP TYPE "Ingredient";

-- CreateTable
CREATE TABLE "Ingredient" (
    "id" SERIAL NOT NULL,
    "name" "IngredientName" NOT NULL,
    "amount" INTEGER NOT NULL,
    "measure" "Measure" NOT NULL,
    "recipeId" TEXT,

    CONSTRAINT "Ingredient_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Ingredient" ADD CONSTRAINT "Ingredient_recipeId_fkey" FOREIGN KEY ("recipeId") REFERENCES "Recipe"("id") ON DELETE SET NULL ON UPDATE CASCADE;
