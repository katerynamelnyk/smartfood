-- CreateEnum
CREATE TYPE "Ingredient" AS ENUM ('cheese', 'eggs', 'flour', 'baking_powder');

-- AlterTable
ALTER TABLE "Recipe" ADD COLUMN     "ingredients" "Ingredient"[];
